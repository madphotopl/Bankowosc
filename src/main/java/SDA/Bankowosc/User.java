package SDA.Bankowosc;

import java.time.LocalDate;

public class User {
    //Imię, nazwisko, adres zamieszkania, data urodzenia oraz pesel
private String name;
private String lastName;
private String addres;
private LocalDate dateOfBirth;
private int pesel;

    public User(String name, String lastName, String addres, LocalDate dateOfBirth, int pesel) {
        this.name = name;
        this.lastName = lastName;
        this.addres = addres;
        this.dateOfBirth = dateOfBirth;
        this.pesel = pesel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddres() {
        return addres;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getPesel() {
        return pesel;
    }

    public void setPesel(int pesel) {
        this.pesel = pesel;
    }
}
